package com.example.crud;

import java.util.ArrayList;
import java.util.List;

public class DataModel {

    private String id;

    private String firstName;

    private String lastName;

    private int age;

    private List DataMahasiswa = new ArrayList();

    public DataModel(){
    }
    public DataModel(String id, String firstName, String lastName, int age) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;

    }
    public int getAge() {
        return age;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getId() {
        return id;
    }

    public String getLastName() {
        return lastName;
    }

    public List getDataMahasiswa() {
        return DataMahasiswa;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setDataMahasiswa(List dataMahasiswa) {
        DataMahasiswa = dataMahasiswa;
    }
}
