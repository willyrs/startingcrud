package com.example.crud;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;


public interface ModuleAPI {
    @GET("contact")
    Call<List<DataModel>> getUsers();

    @POST("contact")
    Call<DataModel> addUser(@Body DataModel user);

    @PUT("contact/{id}")
    Call<DataModel> updateUser(@Path("id") int id, @Body DataModel user);

    @DELETE("contact/{id}")
    Call<DataModel> deleteUser(@Path("id") String id);
}
